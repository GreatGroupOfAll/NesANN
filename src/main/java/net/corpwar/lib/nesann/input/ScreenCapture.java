package net.corpwar.lib.nesann.input;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.*;
import com.sun.jna.win32.W32APIOptions;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

/**
 * JNI implementation to get faster capture
 * nesann
 * Created by Ghost on 2017-02-12.
 */
public class ScreenCapture {


    private int xDelta, yDelta;
    private int width, height;
    private float[] returnValues;
    private BufferedImage bufferedImage;
    private BufferedImage tempBufferedImage;
    private final float min = 0;
    private final float max = 768;

    private WinDef.HWND hWnd;
    private WinDef.RECT bounds;
    private WinGDI.BITMAPINFO bmi;
    private Memory buffer;

    public ScreenCapture(WinDef.HWND hWnd) {
        this.hWnd = hWnd;
    }

    public void setWindow(WinDef.HWND hWnd) {
        this.hWnd = hWnd;
    }

    public void setSize(int width, int height, int x, int y) {
        this.width = width;
        this.height = height;
        this.xDelta = x;
        this.yDelta = y;
        initSomeStuff();
    }

    private void initSomeStuff() {
        returnValues = new float[width*height];
        bounds = new WinDef.RECT();
        bmi = new WinGDI.BITMAPINFO();
        buffer = new Memory(width * height * 8);
        bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    }

    public void captureNewImage() {
        capture(hWnd);
    }

    public int[] getIntWindowData() {
        bufferedImage = tempBufferedImage.getSubimage(0, 0, width, height);
        return ((DataBufferInt)bufferedImage.getRaster().getDataBuffer()).getData();
    }


    public float[] getFloatWindowData() {
        int [] colorArray = ((DataBufferInt) tempBufferedImage.getRaster().getDataBuffer()).getData();
        int i = 0;
        int j = 0;
        int maxT = 0, minT = Integer.MAX_VALUE;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (x >= xDelta && width + xDelta > x && y >= yDelta && yDelta + height > y) {
                    int rgb = colorArray[i];

                    int blue = rgb & 0xff;
                    int green = (rgb & 0xff00) >> 8;
                    int red = (rgb & 0xff0000) >> 16;

                    int newValue = blue + (green) + (red);
                    returnValues[j] = ((newValue - min) / (max - min));
                    j++;
                    if (newValue > maxT) {
                        maxT = newValue;
                    }
                    if (newValue < minT) {
                        minT = newValue;
                    }
                }
                i++;
            }
            if (xDelta + width > tempBufferedImage.getWidth() && y >= yDelta) {
                for (int x = 0; x < tempBufferedImage.getWidth() - width + xDelta; x++) {
                    returnValues[j] = 0;
                    j++;
                }
            }
            if (xDelta + width < tempBufferedImage.getWidth()) {
                i += tempBufferedImage.getWidth() - (xDelta + width);
            }
        }
        return returnValues;
    }

    private void capture(WinDef.HWND hWnd) {

        WinDef.HDC hdcWindow = User32.INSTANCE.GetDC(hWnd);
        WinDef.HDC hdcMemDC = GDI32.INSTANCE.CreateCompatibleDC(hdcWindow);

        User32Extra.INSTANCE.GetClientRect(hWnd, bounds);

        int width = bounds.right - bounds.left;
        int height = bounds.bottom - bounds.top;

        if (tempBufferedImage == null || tempBufferedImage.getHeight() != height || tempBufferedImage.getWidth() != width) {
            tempBufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            buffer = new Memory(width * height * 4);
        }

        WinDef.HBITMAP hBitmap = GDI32.INSTANCE.CreateCompatibleBitmap(hdcWindow, width, height);

        WinNT.HANDLE hOld = GDI32.INSTANCE.SelectObject(hdcMemDC, hBitmap);
        GDI32Extra.INSTANCE.BitBlt(hdcMemDC, 0, 0, width, height, hdcWindow, 0, 0, WinGDIExtra.SRCCOPY);

        GDI32.INSTANCE.SelectObject(hdcMemDC, hOld);
        GDI32.INSTANCE.DeleteDC(hdcMemDC);

        bmi.bmiHeader.biWidth = width;
        bmi.bmiHeader.biHeight = -height;
        bmi.bmiHeader.biPlanes = 1;
        bmi.bmiHeader.biBitCount = 32;
        bmi.bmiHeader.biCompression = WinGDI.BI_RGB;

        GDI32.INSTANCE.GetDIBits(hdcWindow, hBitmap, 0, height, buffer, bmi, WinGDI.DIB_RGB_COLORS);

        tempBufferedImage.setRGB(0, 0, width, height, buffer.getIntArray(0, width * height), 0, width);

        GDI32.INSTANCE.DeleteObject(hBitmap);
        User32.INSTANCE.ReleaseDC(hWnd, hdcWindow);
    }

    // Interfaces for windows JNA
    interface GDI32Extra extends GDI32 {
        GDI32Extra INSTANCE = (GDI32Extra) Native.loadLibrary("gdi32", GDI32Extra.class, W32APIOptions.DEFAULT_OPTIONS);
        boolean BitBlt(WinDef.HDC hObject, int nXDest, int nYDest, int nWidth, int nHeight, WinDef.HDC hObjectSource, int nXSrc, int nYSrc, WinDef.DWORD dwRop);
    }

    interface User32Extra extends User32 {
        User32Extra INSTANCE = (User32Extra) Native.loadLibrary("user32", User32Extra.class, W32APIOptions.DEFAULT_OPTIONS);
        HDC GetWindowDC(HWND hWnd);
        boolean GetClientRect(HWND hWnd, RECT rect);
    }

    interface WinGDIExtra extends WinGDI {
        WinDef.DWORD SRCCOPY = new WinDef.DWORD(0x00CC0020);
    }

}
