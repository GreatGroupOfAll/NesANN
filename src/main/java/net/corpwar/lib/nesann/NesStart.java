package net.corpwar.lib.nesann;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import net.corpwar.lib.nesann.genetic.Genetic;
import net.corpwar.lib.nesann.gui.MainWindow;
import net.corpwar.lib.nesann.input.ScreenCapture;
import net.corpwar.lib.nesann.network.FeedforwardNormalized;
import net.corpwar.lib.nesann.neuralnet.*;
import net.corpwar.lib.nesann.output.Keybord;
import net.corpwar.lib.nesann.scriptsystem.AnnScriptEngine;

import java.util.Random;

/**
 * nesann
 * Created by Ghost on 2017-02-04.
 */
public class NesStart {

    private Random random = new Random();
    private StringBuilder sb = new StringBuilder();
    private MainWindow mainWindow;
    private ScreenCapture screenCapture;
    private Keybord keybord;
    private Genetic genetic;
    private FeedforwardNormalized feedForward;
    private WinDef.HWND hWnd;
    private FileHandle fileHandle;
    private float[] lastFrame;
    private long sameScreenTime;
    private AnnScriptEngine annScriptEngineEngine;

    private NesStart() {
        User32.INSTANCE.EnumWindows(( hWnd, data ) -> {
                    char[] name = new char[512];
                    User32.INSTANCE.GetWindowText(hWnd, name, name.length);
                    if (Native.toString(name).toLowerCase().contains("nestopia")) {
                        this.hWnd = hWnd;
                        return false;
                    }
                    return true;
                }, null);
        initNetwork();
  }

    private void initNetwork() {
        annScriptEngineEngine = new AnnScriptEngine(this);
        keybord = new Keybord();
        fileHandle = new FileHandle();
        genetic = new Genetic(10, this);

        mainWindow = new MainWindow(this);
        screenCapture = new ScreenCapture(hWnd);
    }

    public void setLastFrame(int width, int height) {
        lastFrame = new float[width * height];
    }

    public String runNetwork(int loops, Network network) {
        sb.setLength(0);
        if (network != null) {
            feedForward = new FeedforwardNormalized(network);
        } else {
            feedForward = new FeedforwardNormalized(createNewNetwork());
            genetic.getGeneration().addIndividual(feedForward.getNetwork());
        }
        keybord.resetLevel();

        long startTime = System.currentTimeMillis();
        sameScreenTime = 0;
        long sameScreenTimeLastTime = System.currentTimeMillis();
        long fitnessTotalTime = 0;
        mainWindow.getRawImage().setNetwork(feedForward.getNetwork());
        while (startTime + (loops * 1000) > System.currentTimeMillis()) {
            long fitnessTempTime = System.currentTimeMillis();
            screenCapture.captureNewImage();
            float[] setValues = screenCapture.getFloatWindowData();
            System.arraycopy(setValues, 0, feedForward.getInputValues(), 0, feedForward.getInputValues().length);
            feedForward.runFeedForward();
            keybord.pressAKey(feedForward.getNetwork().getLayers().get(feedForward.getNetwork().getLayers().size()-1));

            mainWindow.getRawImage().setColors(feedForward.getNetwork().getLayers().get(0));

            float procentSameScreen = procentCompareTwoFrames(setValues, lastFrame);
            if (procentSameScreen > 0.97) {
                sameScreenTime = System.currentTimeMillis() - sameScreenTimeLastTime;
            } else {
                sameScreenTimeLastTime = System.currentTimeMillis();
                fitnessTotalTime += System.currentTimeMillis() - fitnessTempTime;
            }
            if (sameScreenTime > 10000) {
                break;
            }
            if (isBlackScreen(setValues)) {
                break;
            }
            System.arraycopy( setValues, 0, lastFrame, 0, setValues.length );
        }

        keybord.resetKeys();
        long totalTime = (System.currentTimeMillis() - startTime);
        sb.append("TotalTime: ").append(totalTime).append(" Avg: ").append(totalTime / loops).append(" fitFunc: ").append(fitnessTotalTime);
        genetic.getGeneration().updateFitness(fitnessTotalTime, feedForward.getNetwork());

        return sb.toString();
    }

    private float procentCompareTwoFrames(float[] newFrame, float[] oldFrame) {
        float sumSameValues = 0;
        if (newFrame.length != oldFrame.length) {
            return -1;
        } else {
            for (int i = 0; i < newFrame.length; i++) {
                if (newFrame[i] == oldFrame[i]) {
                    sumSameValues++;
                }
            }
            return sumSameValues / newFrame.length;
        }
    }

    private boolean isBlackScreen(float[] newFrame) {
        float sumSameValues = 0;
        float min = Float.MAX_VALUE, max = 0;
        for (float aNewFrame : newFrame) {
            if (aNewFrame <= 0.0001) {
                sumSameValues++;
            }
            if (aNewFrame < min) {
                min = aNewFrame;
            }
            if (aNewFrame > max) {
                max = aNewFrame;
            }
        }
        return sumSameValues > newFrame.length * 0.8;
    }

    public Genetic getGenetic() {
        return genetic;
    }

    public FileHandle getFileHandle() {
        return fileHandle;
    }

    public Keybord getKeybord() {
        return keybord;
    }

    public MainWindow getMainWindow() {
        return mainWindow;
    }

    public AnnScriptEngine getAnnScriptEngineEngine() {
        return annScriptEngineEngine;
    }

    public ScreenCapture getScreenCapture() {
        return screenCapture;
    }

    public Network createNewNetwork() {
        return annScriptEngineEngine.createNetworkScript();
    }

    public static void main(String[] args) {
        new NesStart();
    }
}
