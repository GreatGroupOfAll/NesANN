package net.corpwar.lib.nesann.output;

import com.sun.jna.platform.win32.BaseTSD;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinUser;
import net.corpwar.lib.nesann.neuralnet.Layer;
import net.corpwar.lib.nesann.neuralnet.Neuron;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * nesann
 * Created by Ghost on 2017-02-19.
 */
public class Keybord {


    private Robot robot;
    private long abuttonPressTime;
    private long bbuttonPressTime;
    private long currentTime = System.currentTimeMillis();

    private boolean pressLeft = false;
    private boolean pressRight = false;
    private boolean pressUp = false;
    private boolean pressDown = false;
    private boolean pressAButton = false;
    private boolean pressBButton = false;
    private float activation = 0.57f;


    public Keybord() {
        try {
            robot = new Robot();
        } catch (AWTException e) {
            // We hope it will work
        }
    }

    public void setActivation(float activation) {
        this.activation = activation;
    }

    public float getActivation() {
        return activation;
    }

    public void resetLevel() {
        robot.keyPress(KeyEvent.VK_1);
        robot.keyRelease(KeyEvent.VK_1);
    }

    public void pressAKey(Layer outputLayer) {

        for (int i = 0; i < outputLayer.getAllNeuronsOnLayer().size(); i++) {
            Neuron neuron = outputLayer.getAllNeuronsOnLayer().get(i);
            currentTime = System.currentTimeMillis();

            if (neuron.getName().equalsIgnoreCase("AButton")) {
                if (!pressAButton && neuron.getLastSum() > activation) {
                    robot.keyPress(KeyEvent.VK_PERIOD);
                    abuttonPressTime = currentTime;
                    pressAButton = !pressAButton;
                } else if (pressAButton && neuron.getLastSum() <= activation) {
                    robot.keyRelease(KeyEvent.VK_PERIOD);
                    pressAButton = !pressAButton;
                }
                if (pressAButton && abuttonPressTime + 500 < currentTime) {
                    robot.keyRelease(KeyEvent.VK_PERIOD);
                    pressAButton = !pressAButton;
                }
                continue;
            }

            if (neuron.getName().equalsIgnoreCase("BButton")) {
                if (!pressBButton && neuron.getLastSum() > activation) {
                    robot.keyPress(KeyEvent.VK_COMMA);
                    bbuttonPressTime = currentTime;
                    pressBButton = !pressBButton;
                } else if (pressBButton && neuron.getLastSum() <= activation) {
                    robot.keyRelease(KeyEvent.VK_COMMA);
                    pressBButton = !pressBButton;
                }
                continue;
            }

            if (neuron.getName().equalsIgnoreCase("Right")) {
                if (!pressRight && neuron.getLastSum() > activation) {
                    robot.keyPress(KeyEvent.VK_L);
                    pressRight = !pressRight;
                } else if (pressRight && neuron.getLastSum() <= activation) {
                    robot.keyRelease(KeyEvent.VK_L);
                    pressRight = !pressRight;
                }
                continue;
            }

            if (neuron.getName().equalsIgnoreCase("Left")) {
                if (!pressLeft && neuron.getLastSum() > activation) {
                    robot.keyPress(KeyEvent.VK_J);
                    pressLeft = !pressLeft;
                } else if (pressLeft && neuron.getLastSum() <= activation) {
                    robot.keyRelease(KeyEvent.VK_J);
                    pressLeft = !pressLeft;
                }
                continue;
            }

            if (neuron.getName().equalsIgnoreCase("Up")) {
                if (!pressUp && neuron.getLastSum() > activation) {
                    robot.keyPress(KeyEvent.VK_I);
                    pressUp = !pressUp;
                } else if (pressUp && neuron.getLastSum() <= activation) {
                    robot.keyRelease(KeyEvent.VK_I);
                    pressUp = !pressUp;
                }
                continue;
            }

            if (neuron.getName().equalsIgnoreCase("Down")) {
                if (!pressDown && neuron.getLastSum() > activation) {
                    robot.keyPress(KeyEvent.VK_K);
                    pressDown = !pressDown;
                } else if (pressDown && neuron.getLastSum() <= activation) {
                    robot.keyRelease(KeyEvent.VK_K);
                    pressDown = !pressDown;
                }
            }
        }
    }

    public void resetKeys() {
        robot.keyRelease(KeyEvent.VK_J);
        robot.keyRelease(KeyEvent.VK_L);
        robot.keyRelease(KeyEvent.VK_I);
        robot.keyRelease(KeyEvent.VK_K);
        robot.keyRelease(KeyEvent.VK_COMMA);
        robot.keyRelease(KeyEvent.VK_PERIOD);
        pressLeft = false;
        pressRight = false;
        pressUp = false;
        pressDown = false;
        pressAButton = false;
        pressBButton = false;
    }

}
