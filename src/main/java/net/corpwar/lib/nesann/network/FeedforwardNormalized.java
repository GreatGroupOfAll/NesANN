package net.corpwar.lib.nesann.network;

import net.corpwar.lib.nesann.neuralnet.Network;
import net.corpwar.lib.nesann.neuralnet.Neuron;
import net.corpwar.lib.nesann.neuralnet.activationfunction.Normalization;

import java.util.ArrayList;
import java.util.List;

/**
 * Feed forward a network. The network must only go forward and can't go back.
 * TODO need to check that no connection goes backward in the network
 * Created by Ghost on 2017-02-05.
 */
public class FeedforwardNormalized {

    private Network network;
    private float[] inputValues;
    private float[] outputValues;
    private float softMin, softMax;

    public FeedforwardNormalized(Network network) {
        this.network = network;
        int inputValuesSize = network.getLayers().get(0).getAllNeuronsOnLayer().size();
        inputValues = new float[inputValuesSize];
        for (int i = 0; i < inputValuesSize; i++) {
            inputValues[i] = 0f;
        }

        int outputValuesSize = network.getLayers().get(network.getLayers().size()-1).getAllNeuronsOnLayer().size();
        outputValues = new float[outputValuesSize];
        for (int i = 0; i < outputValuesSize; i++) {
            outputValues[i] = 0f;
        }
    }

    public Network getNetwork() {
        return network;
    }

    public float[] getInputValues() {
        return inputValues;
    }

    // TODO run on GPU
    public void runFeedForward() {
        List<Neuron> inputNeurons = network.getLayers().get(0).getAllNeuronsOnLayer();
        for (int i = 0; i < inputNeurons.size(); i++) {
            if (inputNeurons.get(i).getName().equalsIgnoreCase("bias")) {
                inputNeurons.get(i).fire();
                continue;
            }
            inputNeurons.get(i).setSum(inputValues[i]);
            inputNeurons.get(i).fire();
            inputNeurons.get(i).resetSum();
        }

        setMinMax(1);

        for (int i = 1; i < network.getLayers().size(); i++) {
            List<Neuron> neurons = network.getLayers().get(i).getAllNeuronsOnLayer();
            for (int j = 0; j < neurons.size(); j++) {
                Neuron neuron = neurons.get(j);
                if (neuron.getName().equalsIgnoreCase("bias")) {
                    neuron.fire();
                    continue;
                }

                if (neuron.getToThisConnection().size() != 0) {
                    if (neuron.getActiveFunction() instanceof Normalization) {
                        ((Normalization) neuron.getActiveFunction()).setValues(softMin, softMax);
                    }
                    neuron.runActivationFunction();
                    neuron.fire();
                }
                neuron.resetSum();
            }
            if (i + 1 < network.getLayers().size()) {
                setMinMax(i + 1);
            }
        }

    }

    private void setMinMax(int layer) {
        softMin = 0;
        softMax = 0;
        List<Neuron> neurons = network.getLayers().get(layer).getAllNeuronsOnLayer();
        for (int j = 0; j < neurons.size(); j++) {
            Neuron neuron = neurons.get(j);
            if (neuron.getActiveFunction() instanceof Normalization) {
                if (neuron.getSum() < softMin) {
                    softMin = neuron.getSum();
                }
                if (neuron.getSum() > softMax) {
                    softMax = neuron.getSum();
                }
            }
        }
    }
}
