package net.corpwar.lib.nesann.network;

import net.corpwar.lib.nesann.neuralnet.Network;
import net.corpwar.lib.nesann.neuralnet.Neuron;

import java.util.ArrayList;
import java.util.List;

/**
 * Feed forward a network. The network must only go forward and can't go back.
 * TODO need to check that no connection goes backward in the network
 * Created by Ghost on 2017-02-05.
 */
public class Feedforward {

    private Network network;
    private List<Float> inputValues;

    public Feedforward(Network network) {
        this.network = network;
        int inputValuesSize = network.getLayers().get(0).getAllNeuronsOnLayer().size();
        inputValues = new ArrayList<>(inputValuesSize);
        for (int i = 0; i < inputValuesSize; i++) {
            inputValues.add(0f);
        }
    }

    public List<Float> getInputValues() {
        return inputValues;
    }

    // TODO run on GPU
    public void runFeedForward() {
        List<Neuron> inputNeurons = network.getLayers().get(0).getAllNeuronsOnLayer();
        for (int i = 0; i < inputNeurons.size(); i++) {
            if (inputNeurons.get(i).getName().equalsIgnoreCase("bias")) {
                inputNeurons.get(i).fire();
                continue;
            }
            inputNeurons.get(i).setSum(inputValues.get(i));
            inputNeurons.get(i).fire();
            inputNeurons.get(i).resetSum();
        }

        for (int i = 1; i < network.getLayers().size(); i++) {
            List<Neuron> neurons = network.getLayers().get(i).getAllNeuronsOnLayer();
            for (Neuron neuron : neurons) {
                if (neuron.getName().equalsIgnoreCase("bias")) {
                    neuron.fire();
                    continue;
                }
                //System.out.println("Before cost: " + neuron.getSum());
                //neuron.calculateCostFunction();
                //System.out.println("After cost: " + neuron.getSum());
                if (neuron.getToThisConnection().size() != 0) {
                    neuron.fire();
                }
                neuron.resetSum();
            }
        }

    }

}
