package net.corpwar.lib.nesann.gui;

import net.corpwar.lib.nesann.NesStart;
import net.corpwar.lib.nesann.neuralnet.Network;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.regex.Pattern;

/**
 * nesann
 * Created by Ghost on 2017-02-05.
 */
public class MainWindow extends JFrame{
    private JTextField txtOutput;
    private JButton btnTrain;
    private JPanel rootPanel;
    private JTextField txtInputLoops;
    private ImageInputPanel rawImage;
    private JTextField txtIndividuals;
    private JTextField txtGenerations;
    private JButton btnRunNetwork;
    private JTextField txtActivation;
    private JButton stopButton;
    private JTextArea txtScriptArea;
    private JTextArea txtErrorArea;
    private JCheckBox chbDrawConnection;
    private NesStart nesStart;
    private final JFileChooser fc;
    private Network network;
    private boolean running = false;
    private String strCreateNetwork = "";

    public MainWindow(NesStart nesStart) {
        fc = new JFileChooser();
        this.nesStart = nesStart;
        rawImage.setNesStart(nesStart);
        setContentPane(rootPanel);
        setTitle("ANN for nes");
        setSize(1024, 768);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setStrCreateNetwork();
        txtScriptArea.setText(strCreateNetwork);
        btnTrain.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
            txtOutput.setText("");
            txtErrorArea.setText("");
            if (Pattern.matches("^\\d*$", txtInputLoops.getText()) &&
                    Pattern.matches("^\\d*$", txtIndividuals.getText()) &&
                    Pattern.matches("^\\d*$", txtGenerations.getText())) {
                nesStart.getGenetic().setIndividuals(Integer.parseInt(txtIndividuals.getText()));
                nesStart.getKeybord().setActivation(Float.parseFloat(txtActivation.getText()));
                running = true;
                strCreateNetwork = txtScriptArea.getText();
                new Thread(() -> {
                    nesStart.getGenetic().clearNetworks();
                    txtOutput.setText("Spawning new networks");
                    nesStart.getAnnScriptEngineEngine().evalScript(strCreateNetwork);
                    nesStart.getGenetic().startGenerations();
                    if (!nesStart.getAnnScriptEngineEngine().isScriptError()) {
                        for (int i = 3; i > 0; i--) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ex) {
                                ex.printStackTrace();
                            }
                            txtOutput.setText("Click on nes window: " + i);
                        }
                        for (int j = 0; j < Integer.parseInt(txtGenerations.getText()); j++) {
                            for (int i = 0; i < nesStart.getGenetic().getIndividuals(); i++) {
                                String test = nesStart.runNetwork(Integer.parseInt(txtInputLoops.getText()), nesStart.getGenetic().getGeneration().getIndividualList().get(i).getNetwork());
                                // Runs inside of the Swing UI thread
                                final int finalJ = j;
                                final int finalI = i;
                                SwingUtilities.invokeLater(() -> txtOutput.setText(test + " Gen: " + finalJ + " Ind: " + finalI));
                                if (!running || nesStart.getAnnScriptEngineEngine().isScriptError()) {
                                    break;
                                }
                            }
                            if (!running || nesStart.getAnnScriptEngineEngine().isScriptError()) {
                                break;
                            }
                            nesStart.getGenetic().getBestNetworks();
                            SwingUtilities.invokeLater(() -> txtOutput.setText("Mix generations"));
                            nesStart.getGenetic().spawnNewGeneration();
                        }
                        SwingUtilities.invokeLater(() -> txtOutput.setText("Save best network"));
                        //nesStart.getFileHandle().saveNetwork(nesStart.getGenetic().getBestNetwork().getNetwork(), "bestnet");
                        SwingUtilities.invokeLater(() -> txtOutput.setText("STOP!!"));
                    } else {
                        Thread.currentThread().interrupt();
                    }
                }).start();
            }
            }
        });
        btnRunNetwork.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Pattern.matches("^\\d*$", txtInputLoops.getText())) {
                    int result = fc.showOpenDialog(MainWindow.this);
                    if (result == JFileChooser.APPROVE_OPTION) {
                        File selectedFile = fc.getSelectedFile();
                        txtOutput.setText("Load new file");
                        network = nesStart.getFileHandle().loadNetwork(selectedFile.getAbsolutePath());
                        new Thread(() -> {
                            for (int i = 3; i > 0; i--) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException ex) {
                                    ex.printStackTrace();
                                }
                                txtOutput.setText("Click on nes window: " + i);
                            }
                            nesStart.runNetwork(Integer.parseInt(txtInputLoops.getText()), network);
                            txtOutput.setText("STOP!!");
                        }).start();
                    } else {
                        if (network != null && Pattern.matches("^\\d*$", txtInputLoops.getText())) {
                            new Thread(() -> {
                                for (int i = 3; i > 0; i--) {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException ex) {
                                        ex.printStackTrace();
                                    }
                                    txtOutput.setText("Click on nes window: " + i);
                                }
                                for (int j = 0; j < Integer.parseInt(txtGenerations.getText()); j++) {
                                    nesStart.runNetwork(Integer.parseInt(txtInputLoops.getText()), network);
                                    if (!running || nesStart.getAnnScriptEngineEngine().isScriptError()) {
                                        break;
                                    }
                                }
                                txtOutput.setText("STOP!!");
                            }).start();
                        }
                    }
                }

            }
        });
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                running = false;
            }
        });
        chbDrawConnection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rawImage.setDrawConnection(chbDrawConnection.isSelected());
            }
        });
    }

    public ImageInputPanel getRawImage() {
        return rawImage;
    }

    public JTextArea getScriptArea() {
        return txtScriptArea;
    }

    public JTextArea getErrorArea() {
        return txtErrorArea;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        rawImage = new ImageInputPanel();
    }

    private void setStrCreateNetwork() {
        strCreateNetwork = "var helperClasses = Java.type(\"net.corpwar.lib.nesann.scriptsystem.ScriptFunctions\")\n" +
                "\n" +
                "function createNetwork(network) {\n" +
                "\n" +
                "  helperClasses.addScreen(256, 240, 0, 0, network);\n" +
                "  helperClasses.addNeurons(30, network);\n" +
                "  helperClasses.addNeurons(20, network);\n" +
                "  helperClasses.addNeurons(15, network);\n" +
                "  helperClasses.addOutput(network);\n" +
                "\n" +
                "  for (i = 0; i < network.getLayers().length - 1; i++) {\n" +
                "    for each (var fromNeuron in network.getLayers().get(i).getAllNeuronsOnLayer()) {\n" +
                "      for each (var toNeuron in network.getLayers().get(i + 1).getAllNeuronsOnLayer()) {\n" +
                "        helperClasses.addConnection(fromNeuron, toNeuron, Math.random() - 0.5);\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "  \n" +
                "  for (i = 1; i < network.getLayers().length - 1; i++) {\n" +
                "    for each (var neuron in network.getLayers().get(i).getAllNeuronsOnLayer()) {\n" +
                "      helperClasses.setActivtion(helperClasses.ActivationTypes.SIGMOD, neuron, 0, network.getLayers().get(i - 1).getAllNeuronsOnLayer().length);\n" +
                "    }\n" +
                "  }\n" +
                "  \n" +
                "  for each (var neuron in network.getLayers().get(network.getLayers().length - 1).getAllNeuronsOnLayer()) {\n" +
                "    helperClasses.setActivtion(helperClasses.ActivationTypes.SIGMOD, neuron, 0, 10);\n" +
                "  }\n" +
                "  return network;\n" +
                "}\n" +
                "\n" +
                "function spawnNewGeneration(generatioin) {\n" +
                "  var bestNetwork = helperClasses.getBestNetwork();\n" +
                "  \n" +
                "  for (i = 0; i < generatioin.getIndividualList().size(); i++) {\n" +
                "    var network = generatioin.getIndividualList().get(i).getNetwork();\n" +
                "\n" +
                "    for (var l = network.getLayers().size() - 1; l >= 0; l--) {\n" +
                "      var layer = network.getLayers().get(l);\n" +
                "      for (var n = layer.getAllNeuronsOnLayer().size() - 1; n >= 0; n--) {\n" +
                "        var neuron = layer.getAllNeuronsOnLayer().get(n);\n" +
                "        for (var c = neuron.getToThisConnection().size() - 1; c >= 0; c--) {\n" +
                "          var connection = neuron.getToThisConnection().get(c);\n" +
                "          if (Math.random() >= 0.01) {\n" +
                "            if (Math.random() < 0.60) {\n" +
                "              var bestWeight = bestNetwork.getLayers().get(l).getAllNeuronsOnLayer().get(n).getToThisConnection().get(c).getWeight();\n" +
                "              var randomWeight = (Math.random() - 0.5) / 500;\n" +
                "              connection.setWeight(bestWeight + randomWeight);\n" +
                "            } else {\n" +
                "              connection.setWeight(bestNetwork.getLayers().get(l).getAllNeuronsOnLayer().get(n).getToThisConnection().get(c).getWeight());\n" +
                "            }\n" +
                "          } else {\n" +
                "            connection.setWeight(Math.random() - 0.5);\n" +
                "          }\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
    }

    public String getStrCreateNetwork() {
        return strCreateNetwork;
    }
}
