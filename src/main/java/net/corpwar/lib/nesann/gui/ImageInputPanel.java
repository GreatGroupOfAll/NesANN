package net.corpwar.lib.nesann.gui;

import net.corpwar.lib.nesann.NesStart;
import net.corpwar.lib.nesann.neuralnet.Connection;
import net.corpwar.lib.nesann.neuralnet.Layer;
import net.corpwar.lib.nesann.neuralnet.Network;
import net.corpwar.lib.nesann.neuralnet.Neuron;


import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * nesann
 * Created by Ghost on 2017-02-11.
 */
public class ImageInputPanel extends JPanel {

    private float[][] colors;
    private NesStart nesStart;
    private Network network;
    private static Font sanSerifFont = new Font("SanSerif", Font.PLAIN, 12);
    private Map<Neuron, XYCord> neuronXYCordHashMap = new HashMap<>();
    private int width, height;
    private boolean drawConnection = false;

    public void setNesStart(NesStart nesStart) {
        this.nesStart = nesStart;
    }

    public void setWidthHeight(int width, int height) {
        this.width = width;
        this.height = height;
        colors = new float[width][height];
    }

    public void setNetwork(Network network) {
        this.network = network;
        for (int j = 1; j < network.getLayers().size(); j++) {
            for (int k = 0; k < network.getLayers().get(j).getAllNeuronsOnLayer().size(); k++) {
                Neuron neuron = network.getLayers().get(j).getAllNeuronsOnLayer().get(k);
                neuronXYCordHashMap.put(neuron, new XYCord(j, k));
            }
        }
    }

    public void setColors(Layer input) {
        int k = 0;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                colors[x][y] = input.getAllNeuronsOnLayer().get(k++).getLastSum();
           }
        }
        repaint();
    }

    public void setDrawConnection(boolean drawConnection) {
        this.drawConnection = drawConnection;
    }

    private void doDrawing(Graphics g) {
        if (network == null) {
            return;
        }

        Graphics2D g2d = (Graphics2D) g;

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                g2d.setPaint(Color.getHSBColor(colors[x][y], 1, 1));
                g2d.drawLine(x,y,x,y);
            }
        }

        for (int j = 1; j < network.getLayers().size(); j++) {
            for (int k = 0; k < network.getLayers().get(j).getAllNeuronsOnLayer().size(); k++) {
                Neuron neuron = network.getLayers().get(j).getAllNeuronsOnLayer().get(k);
                if (drawConnection) {
                    if (neuron.getFromThisConnection() != null) {
                        for (Connection connection : neuron.getFromThisConnection()) {
                            XYCord from = neuronXYCordHashMap.get(connection.getFromNeuron());
                            XYCord to = neuronXYCordHashMap.get(connection.getToNeuron());
                            float color = Math.abs(connection.getWeight());
                            float aboveOne = color;
                            float belowZero = color;
                            if (color > 1) {
                                color = 1;
                                aboveOne = 0;
                                belowZero = 1;
                            }
                            if (color < 0) {
                                color = 0;
                                aboveOne = 0;
                                belowZero = 1;
                            }
                            g2d.setColor(new Color(belowZero, aboveOne, color));
                            g2d.drawLine(263 + (from.x * 20), from.y * 8, 260 + (to.x * 20), to.y * 8);
                        }
                    }
                }
                //System.out.println("Neuron: " + neuron.getLastSum() + " Layer: " + j + " Neuron: " + k);
                g2d.setPaint(Color.getHSBColor(neuron.getLastSum(), 1, 1));
                g2d.fillRect(260 + (j * 20), k * 8, 3, 3);
            }
        }

        int a = network.getLayers().size() - 1;
        for (int k = 0; k < network.getLayers().get(a).getAllNeuronsOnLayer().size(); k++) {
            Neuron neuron = network.getLayers().get(a).getAllNeuronsOnLayer().get(k);
            g2d.setPaint(Color.getHSBColor(neuron.getLastSum(), 1, 1));
            g2d.fillRect(260 + (network.getLayers().size() * 20), k * 15,13, 13);

            if (neuron.getLastSum() > nesStart.getKeybord().getActivation()) {
                g2d.setPaint(Color.getHSBColor(0.5f, 1, 1));
            } else {
                g2d.setPaint(Color.getHSBColor(0, 1, 1));
            }
            g.setFont(sanSerifFont);
            g.drawString(neuron.getName() + " " + neuron.getLastSum(), 260 + (network.getLayers().size() * 20) + 16, k * 15 + 13);
        }

        g2d.setPaint(Color.getHSBColor(0.5f, 1, 1));
        g.setFont(sanSerifFont);
        g.drawString("Key aktivation: " + nesStart.getKeybord().getActivation(), 260 + (network.getLayers().size() * 20) + 16, network.getLayers().get(a).getAllNeuronsOnLayer().size() * 15 + 15);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        doDrawing(g);
    }

    private class XYCord {
        int x;
        int y;

        public XYCord(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

}
