package net.corpwar.lib.nesann.genetic;

import net.corpwar.lib.nesann.neuralnet.Network;

import java.util.ArrayList;
import java.util.List;

/**
 * nesann
 * Created by Ghost on 2017-03-04.
 */
public class Generation {

    private List<Individual> individualList = new ArrayList<>();

    public void addIndividual(Network network) {
        individualList.add(new Individual(-1, network));
    }

    public void removeIndividual(Network network) {
        for (Individual individual : individualList) {
            if (individual.getNetwork().equals(network)) {
                individualList.remove(individual);
                break;
            }
        }
    }

    public void updateFitness(long fitness, Network network) {
        for (Individual individual : individualList) {
            if (individual.getNetwork().equals(network)) {
                individual.updateFitness(fitness);
                break;
            }
        }
    }

    public List<Individual> getIndividualList() {
        return individualList;
    }

}
