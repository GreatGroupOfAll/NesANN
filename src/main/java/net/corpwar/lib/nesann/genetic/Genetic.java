package net.corpwar.lib.nesann.genetic;

import net.corpwar.lib.nesann.NesStart;
import net.corpwar.lib.nesann.neuralnet.Connection;
import net.corpwar.lib.nesann.neuralnet.Layer;
import net.corpwar.lib.nesann.neuralnet.Network;
import net.corpwar.lib.nesann.neuralnet.Neuron;

import java.util.Random;

/**
 * nesann
 * Created by Ghost on 2017-03-04.
 */
public class Genetic {

    private Generation generation = new Generation();
    private Individual bestNetwork;
    private Integer amountOfIndividualsInGeneration;
    private NesStart nesStart;

    public Genetic(Integer individuals, NesStart nesStart) {
        this.amountOfIndividualsInGeneration = individuals;
        this.nesStart = nesStart;
    }

    public Generation getGeneration() {
        return generation;
    }

    public void getBestNetworks() {
        for (int i = generation.getIndividualList().size() - 1; i >= 0; i--) {
            if (nesStart.getAnnScriptEngineEngine().isScriptError()) {
                break;
            }
            Individual individual = generation.getIndividualList().get(i);
            if (bestNetwork == null) {
                bestNetwork = new Individual(-1, nesStart.createNewNetwork());
                bestNetwork.getNetwork().setWeightFromOtherNetwork(individual.getNetwork());
                continue;
            }
            if (individual.getFitnessValue() >= bestNetwork.getFitnessValue()) {
                bestNetwork.getNetwork().setWeightFromOtherNetwork(individual.getNetwork());
                bestNetwork.updateFitness(individual.getFitnessValue());
            }
        }
    }

    public void startGenerations() {
        generation.getIndividualList().clear();
        bestNetwork = new Individual(-1, nesStart.createNewNetwork());
        for (int i = 0; i < amountOfIndividualsInGeneration; i++) {
            generation.addIndividual(nesStart.getAnnScriptEngineEngine().createNetworkScript());
        }
    }

    public void clearNetworks() {
        bestNetwork = null;
        generation.getIndividualList().clear();
    }

    public void spawnNewGeneration() {
        nesStart.getAnnScriptEngineEngine().spawnNewGenerations(generation);
    }

    public Individual getBestNetwork() {
        getBestNetworks();
        return bestNetwork;
    }

    public Integer getIndividuals() {
        return amountOfIndividualsInGeneration;
    }

    public void setIndividuals(Integer individuals) {
        amountOfIndividualsInGeneration = individuals;
    }

}
