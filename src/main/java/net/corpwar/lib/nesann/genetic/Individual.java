package net.corpwar.lib.nesann.genetic;

import net.corpwar.lib.nesann.neuralnet.Network;

/**
 * nesann
 * Created by Ghost on 2017-03-04.
 */
public class Individual {

    private long fitnessValue;
    private Network network;

    public Individual(Integer fitnessValue, Network network) {
        this.fitnessValue = fitnessValue;
        this.network = network;
    }

    public void updateFitness(long fitness) {
        fitnessValue = fitness;
    }

    public long getFitnessValue() {
        return fitnessValue;
    }

    public Network getNetwork() {
        return network;
    }
}
