package net.corpwar.lib.nesann.scriptsystem;

import net.corpwar.lib.nesann.NesStart;
import net.corpwar.lib.nesann.genetic.Individual;
import net.corpwar.lib.nesann.neuralnet.Connection;
import net.corpwar.lib.nesann.neuralnet.Layer;
import net.corpwar.lib.nesann.neuralnet.Network;
import net.corpwar.lib.nesann.neuralnet.Neuron;
import net.corpwar.lib.nesann.neuralnet.activationfunction.NormalSigmodActive;
import net.corpwar.lib.nesann.neuralnet.activationfunction.Normalization;
import net.corpwar.lib.nesann.neuralnet.activationfunction.SigmodActivation;

import java.util.List;

/**
 * nesann
 * Created by Ghost on 2017-03-26.
 */
public class ScriptFunctions {

    private static NesStart nesStart;

    public ScriptFunctions(NesStart nesStart) {
        ScriptFunctions.nesStart = nesStart;
    }

    public static Layer createLayer() {
        return new Layer();
    }

    public static void addScreen(int width, int height, int x, int y, Network network) {
        int neuronsInput = width*height;
        Layer input = new Layer();
        // Screen size in
        for (int i = 0; i < neuronsInput; i++) {
            input.addNeuronToLayer(new Neuron(false));
        }
        network.addLayer(input, 0);
        nesStart.getScreenCapture().setSize(width, height, x, y);
        nesStart.getMainWindow().getRawImage().setWidthHeight(width, height);
        nesStart.setLastFrame(width, height);
    }

    public static void addNeurons(int amountOfNeurons, Network network) {
        Layer hidden = new Layer();
        for (int i = 0; i < amountOfNeurons; i++) {
            hidden.addNeuronToLayer(new Neuron());
        }
        network.addLayer(hidden);
    }

    public static Neuron createNeuron(String name, float sum, boolean setSum) {
        return new Neuron(name, sum, setSum);
    }

    public static void addOutput(Network network) {
        Layer output = new Layer();
        output.addNeuronToLayer(new Neuron("AButton"));
        output.addNeuronToLayer(new Neuron("BButton"));
        output.addNeuronToLayer(new Neuron("Left"));
        output.addNeuronToLayer(new Neuron("Right"));
        output.addNeuronToLayer(new Neuron("Up"));
        output.addNeuronToLayer(new Neuron("Down"));
        output.addNeuronToLayer(new Neuron("Start"));
        output.addNeuronToLayer(new Neuron("Select"));
        network.addLayer(output);
    }

    public static void addConnection(Neuron fromNeuron, Neuron toNeuron, float weight) {
        new Connection(fromNeuron, toNeuron, weight);
    }

    public enum ActivationTypes {
        NORMALIZATION,
        NORMALIZATION_SIGMOD,
        SIGMOD
    }

    public static void setActivtion(ActivationTypes activation, Neuron neuron, Object... args) {
        switch (activation) {

            case NORMALIZATION:
                if (args.length == 2) {
                    neuron.setActiveFunction(new Normalization(Float.valueOf(args[0].toString()), Float.valueOf(args[1].toString())));
                }
                break;
            case NORMALIZATION_SIGMOD:
                if (args.length == 2) {
                    neuron.setActiveFunction(new NormalSigmodActive(Float.valueOf(args[0].toString()), Float.valueOf(args[1].toString())));
                }
                break;
            case SIGMOD:
                neuron.setActiveFunction(new SigmodActivation());
                break;
        }
    }

    public static List<Individual> getIndividualList() {
        return nesStart.getGenetic().getGeneration().getIndividualList();
    }

    public static Network getBestNetwork() {
        return nesStart.getGenetic().getBestNetwork().getNetwork();
    }

    public static Individual getBestIndividual() {
        return nesStart.getGenetic().getBestNetwork();
    }

    public static void print(String str) {
        nesStart.getMainWindow().getErrorArea().append(str + "\n");
    }
}
