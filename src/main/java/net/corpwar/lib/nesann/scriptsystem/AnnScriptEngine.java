package net.corpwar.lib.nesann.scriptsystem;

import net.corpwar.lib.nesann.NesStart;
import net.corpwar.lib.nesann.genetic.Generation;
import net.corpwar.lib.nesann.neuralnet.Network;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * nesann
 * Created by Ghost on 2017-03-26.
 */
public class AnnScriptEngine {

    private ScriptEngine engine;
    private NesStart nesStart;
    private boolean scriptError = false;
    private ReturnFromScript returnFromScript = new ReturnFromScript();

    public AnnScriptEngine(NesStart nesStart) {
        this.nesStart = nesStart;
        new ScriptFunctions(nesStart);
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        engine = scriptEngineManager.getEngineByName("nashorn");
    }

    public void evalScript(String script) {
        try {
            engine.eval(script);
        } catch (ScriptException e) {
            nesStart.getMainWindow().getErrorArea().append("\n" + e.getMessage());
            scriptError = true;
        }
    }

    public Network createNetworkScript() {
        scriptError = false;
        Network network = new Network();
        try {
            Invocable invocable = (Invocable) engine;
            Object returnNetwork = invocable.invokeFunction("createNetwork", network);
            if (returnNetwork instanceof Network) {
                returnFromScript.network = (Network) returnNetwork;
            }



        } catch(Exception e) {
            nesStart.getMainWindow().getErrorArea().append("\n" + e.getMessage());
            scriptError = true;
        }
      
        return network;
    }

    public void spawnNewGenerations(Generation generation) {
        Invocable invocable = (Invocable) engine;
        try {
            invocable.invokeFunction("spawnNewGeneration", generation);

        } catch (Exception e) {
            nesStart.getMainWindow().getErrorArea().append("\n" + e.getMessage());
            scriptError = true;
        }
    }

    public boolean isScriptError() {
        return scriptError;
    }
}
