package net.corpwar.lib.nesann.neuralnet.activationfunction;

/**
 * nesann
 * Created by Ghost on 2017-03-11.
 */
public class NormalSigmodActive extends ActiveFunction {

    private float min;
    private float max;

    public NormalSigmodActive(float min, float max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public float activationFunction(float input) {
        return (float) (1 / (1 + Math.exp(-((input - min) / (max - min)))));
    }

    @Override
    public float derivativeActivationFunction(float input) {
        return 0;
    }
}
