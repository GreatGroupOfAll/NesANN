package net.corpwar.lib.nesann.neuralnet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Convenient class to group neurons in different layers
 * Created by Ghost on 2017-02-04.
 */
public class Layer implements Serializable {

    private List<Neuron> neurons = new ArrayList<>();

    public void addNeuronToLayer(Neuron neuronToAdd) {
        neurons.add(neuronToAdd);
    }

    public void removeNeuronOnLayer(Neuron neuronToRemove) {
        neurons.remove(neuronToRemove);
    }

    public List<Neuron> getAllNeuronsOnLayer() {
        return neurons;
    }
}
