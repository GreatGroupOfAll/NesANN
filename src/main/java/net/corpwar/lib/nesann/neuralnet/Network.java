package net.corpwar.lib.nesann.neuralnet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Network that connect everything together
 * Created by Ghost on 2017-02-05.
 */
public class Network implements Serializable {

    private List<Layer> layerList = new ArrayList<>();

    public void addLayer(Layer layerToAdd) {
            layerList.add(layerToAdd);
    }

    public void addLayer(Layer layerToAdd, int index) {
        layerList.add(index, layerToAdd);
    }

    public List<Layer> getLayers() {
        return layerList;
    }

    public void setWeightFromOtherNetwork(Network inNetwork) {
        for (int l = getLayers().size() - 1; l >= 0; l--) {
            Layer layer = getLayers().get(l);
            for (int n = layer.getAllNeuronsOnLayer().size() - 1; n >= 0; n--) {
                Neuron neuron = layer.getAllNeuronsOnLayer().get(n);
                for (int c = neuron.getToThisConnection().size() - 1; c >= 0; c--) {
                    Connection connection = neuron.getToThisConnection().get(c);
                    float inNetworkWeight = inNetwork.getLayers().get(l).getAllNeuronsOnLayer().get(n).getToThisConnection().get(c).getWeight();
                    connection.setWeight(inNetworkWeight);
                }
            }
        }
    }

}
