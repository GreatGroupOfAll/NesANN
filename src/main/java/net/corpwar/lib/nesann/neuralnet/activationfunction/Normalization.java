package net.corpwar.lib.nesann.neuralnet.activationfunction;

/**
 * nesann
 * Created by Ghost on 2017-02-19.
 */
public class Normalization extends ActiveFunction {

    private float min;
    private float max;

    public Normalization(float min, float max) {
        this.min = min;
        this.max = max;
    }

    public void setValues(float min, float max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public float activationFunction(float input) {
        return ((input - min) / (max - min));
    }

    @Override
    public float derivativeActivationFunction(float input) {
        return 0;
    }
}
