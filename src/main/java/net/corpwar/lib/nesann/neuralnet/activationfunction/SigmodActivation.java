package net.corpwar.lib.nesann.neuralnet.activationfunction;

/**
 * nesann
 * Created by Ghost on 2017-02-18.
 */
public class SigmodActivation extends ActiveFunction {

    @Override
    public float activationFunction(float input) {
        return (float) (1 / (1 + Math.exp(-input)));
    }

    @Override
    public float derivativeActivationFunction(float input) {
        return input*(1-input);
    }
}
