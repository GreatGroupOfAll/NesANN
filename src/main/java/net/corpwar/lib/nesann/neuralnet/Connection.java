package net.corpwar.lib.nesann.neuralnet;

import java.io.Serializable;

/**
 * Keep the connection between neurons and what weight they have
 * Created by Ghost on 2017-02-04.
 */
public class Connection implements Serializable {

    private Neuron fromNeuron;
    private Neuron toNeuron;
    private float weight;

    public Connection(Neuron fromNeuron, Neuron toNeuron, float weight) {
        this.fromNeuron = fromNeuron;
        this.toNeuron = toNeuron;
        this.weight = weight;
        if (fromNeuron != null) {
            fromNeuron.addFromThisConnection(this);
        }
        if (toNeuron != null) {
            toNeuron.addToThisConnection(this);
        }
    }

    public void addInputValue(float value) {
        toNeuron.addInputValue(value * weight);
    }

    public Neuron getFromNeuron() {
        return fromNeuron;
    }

    public void setFromNeuron(Neuron fromNeuron) {
        this.fromNeuron = fromNeuron;
    }

    public Neuron getToNeuron() {
        return toNeuron;
    }

    public void setToNeuron(Neuron toNeuron) {
        this.toNeuron = toNeuron;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}
