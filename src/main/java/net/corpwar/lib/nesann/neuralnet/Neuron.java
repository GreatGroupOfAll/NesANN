package net.corpwar.lib.nesann.neuralnet;

import net.corpwar.lib.nesann.neuralnet.activationfunction.ActiveFunction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Neuron add all the inputs and keep a sum of it.
 * Calculate some cost function like sigmod
 * Created by Ghost on 2017-02-04.
 */
public class Neuron implements Serializable {

    private String name = "";
    private float sum;
    private boolean setSum = true;
    private float lastSum;
    private float deltaError;
    private List<Connection> toThisConnection = new ArrayList<>();
    private List<Connection> fromThisConnection = new ArrayList<>();
    private ActiveFunction activeFunction;

    public Neuron() {
        this("");
    }

    public Neuron(String name) {
        this(name, 0);
    }

    public Neuron(boolean setSum) {
        this("", 0, setSum);
    }

    public Neuron(String name, float sum) {
        this(name, sum, true);
    }

    public Neuron(String name, float sum, boolean setSum) {
        this.name = name;
        this.sum = sum;
        this.setSum = setSum;
    }

    public void setActiveFunction(ActiveFunction activeFunction) {
        this.activeFunction = activeFunction;
    }

    public ActiveFunction getActiveFunction() {
        return activeFunction;
    }

    public void addInputValue(float value) {
        sum += value;
    }

    public List<Connection> getFromThisConnection() {
        return fromThisConnection;
    }

    public List<Connection> getToThisConnection() {
        return toThisConnection;
    }

    public void addToThisConnection(Connection connection) {
        toThisConnection.add(connection);
    }

    public void addFromThisConnection(Connection connection) {
        fromThisConnection.add(connection);
    }

    public float getLastSum() {
        return this.lastSum;
    }

    public void setSum(float sum) {
        this.sum = sum;
    }

    public float getSum() {
        return this.sum;
    }

    public void fire() {
        for (int i = 0; i < fromThisConnection.size(); i++) {
            Connection connection = fromThisConnection.get(i);
            connection.addInputValue(sum);
        }
    }
    public void resetSum() {
        lastSum = sum;
        this.sum = 0;
    }

    public float getDeltaError() {
        return this.deltaError;
    }

    public void setDeltaError(float deltaError) {
        this.deltaError = deltaError;
    }

    public String getName() {
        return this.name;
    }

    public void runActivationFunction() {
        if (activeFunction != null) {
            sum = activeFunction.activationFunction(sum);
        }
    }

    public void runDerivativeFunction() {
        if (activeFunction != null) {
            sum = activeFunction.derivativeActivationFunction(sum);
        }
    }
}
