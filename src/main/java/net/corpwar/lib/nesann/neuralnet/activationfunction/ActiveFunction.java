package net.corpwar.lib.nesann.neuralnet.activationfunction;

import java.io.Serializable;

/**
 * nesann
 * Created by Ghost on 2017-02-18.
 */
public abstract class ActiveFunction implements Serializable {

    public abstract float activationFunction(float input);

    public abstract float derivativeActivationFunction(float input);
}
