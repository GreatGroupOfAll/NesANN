package net.corpwar.lib.nesann.neuralnet;

import java.io.*;

/**
 * nesann
 * Created by Ghost on 2017-03-01.
 */
public class FileHandle {

    public void saveNetwork(Network network, String filename) {
        // write object to file
        ObjectOutputStream objectOutputStream = null;
        try {
            RandomAccessFile raf = new RandomAccessFile(filename + ".ann", "rw");
            FileOutputStream fos = new FileOutputStream(raf.getFD());
            objectOutputStream = new ObjectOutputStream(fos);
            objectOutputStream.writeObject(network);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Network loadNetwork(String filename) {
        // read object from file
        try {
            FileInputStream fis = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Network network = (Network) ois.readObject();
            ois.close();
            return network;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
