# Nes Artificial Neural Network

[![build status](https://gitlab.com/GreatGroupOfAll/NesANN/badges/master/build.svg)](https://gitlab.com/GreatGroupOfAll/NesANN/commits/master) [![Chat on Gitter](http://www.corpwar.net/wp-content/uploads/2017/03/gitter.png)](https://gitter.im/CorpWarNet/NesANN)



I have all the basic working to train a network to play NES games.

I haven't done any executable that you can just pick up so you have to grab the source and compile it your self at the moment.

## Screenshot of the GUI
![NesANN](http://www.corpwar.net/wp-content/uploads/2017/03/nesANN2.png)